EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pspice:VSOURCE V1
U 1 1 6100E8E6
P 4100 3230
F 0 "V1" H 4328 3276 50  0000 L CNN
F 1 "5V" H 4328 3185 50  0000 L CNN
F 2 "" H 4100 3230 50  0001 C CNN
F 3 "~" H 4100 3230 50  0001 C CNN
	1    4100 3230
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 3530 4100 3910
Wire Wire Line
	4100 2930 4100 2440
Wire Wire Line
	4100 3910 4700 3910
$Comp
L pspice:0 #GND01
U 1 1 61019204
P 4700 4110
F 0 "#GND01" H 4700 4010 50  0001 C CNN
F 1 "0" H 4700 4199 50  0000 C CNN
F 2 "" H 4700 4110 50  0001 C CNN
F 3 "~" H 4700 4110 50  0001 C CNN
	1    4700 4110
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 4110 4700 3910
Connection ~ 4700 3910
Wire Wire Line
	5760 2440 5760 2840
$Comp
L Device:Q_NMOS_DGS Q1
U 1 1 61007BE0
P 5660 3040
F 0 "Q1" H 5865 3086 50  0000 L CNN
F 1 "Q_NMOS_DGS IRF6618" H 5865 2995 50  0000 L CNN
F 2 "" H 5860 3140 50  0001 C CNN
F 3 "~" H 5660 3040 50  0001 C CNN
F 4 "M" H 5660 3040 50  0001 C CNN "Spice_Primitive"
F 5 "IRF6618" H 5660 3040 50  0001 C CNN "Spice_Model"
F 6 "Y" H 5660 3040 50  0001 C CNN "Spice_Netlist_Enabled"
F 7 "MOS.lib" H 5660 3040 50  0001 C CNN "Spice_Lib_File"
	1    5660 3040
	1    0    0    -1  
$EndComp
$Comp
L pspice:R R2
U 1 1 610098E5
P 5760 3550
F 0 "R2" H 5828 3596 50  0000 L CNN
F 1 "22" H 5828 3505 50  0000 L CNN
F 2 "" H 5760 3550 50  0001 C CNN
F 3 "~" H 5760 3550 50  0001 C CNN
	1    5760 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5760 3300 5760 3270
Wire Wire Line
	5760 3800 5760 3910
Wire Wire Line
	4100 2440 5760 2440
Wire Wire Line
	4700 3910 4970 3910
$Comp
L pspice:VSOURCE V2
U 1 1 6100891F
P 4970 3340
F 0 "V2" H 5198 3386 50  0000 L CNN
F 1 "5V" H 5198 3295 50  0000 L CNN
F 2 "" H 4970 3340 50  0001 C CNN
F 3 "~" H 4970 3340 50  0001 C CNN
	1    4970 3340
	1    0    0    -1  
$EndComp
Wire Wire Line
	4970 3040 5460 3040
Wire Wire Line
	4970 3640 4970 3910
Connection ~ 4970 3910
Wire Wire Line
	4970 3910 5760 3910
Wire Wire Line
	5760 3270 6020 3270
Connection ~ 5760 3270
Wire Wire Line
	5760 3270 5760 3240
Text GLabel 6020 3270 2    50   Input ~ 0
OUT
$EndSCHEMATC
